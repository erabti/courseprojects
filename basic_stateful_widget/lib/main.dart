import 'package:flutter/material.dart';

void main() {
  runApp(AppBase());
}

class AppBase extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Lets Flutter"),
          centerTitle: true,
        ),
        body: Center(
          child: MyApp(true),
        ),
      ),
    );
  }
}

class MyApp extends StatefulWidget {
  final bool isCircular;
  MyApp(this.isCircular);
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var isRed = true;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Container(
          width: 150,
          decoration: BoxDecoration(
            color: isRed ? Colors.red : Colors.blue,
            shape: widget.isCircular ? BoxShape.circle : BoxShape.rectangle,
          ),
          height: 150,
        ),
        RaisedButton(
          onPressed: () {
            setState(() {
              isRed = !isRed;
            });
          },
          child: Text(
            "Toggle",
            style: TextStyle(
              color: Colors.white,
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
          color: Colors.blue.shade200,
        )
      ],
    );
  }
}
