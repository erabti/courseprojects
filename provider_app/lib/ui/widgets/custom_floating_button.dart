import 'package:flutter/material.dart';
import 'package:future_app/provider/counter_provider.dart';
import 'package:provider/provider.dart';

class CustomFloatingButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final counter = Provider.of<CounterProvider>(context);
    return FloatingActionButton(
      onPressed: () => counter.increment(),
      child: Icon(Icons.add),
    );
  }
}
