import 'package:flutter/material.dart';
import 'package:future_app/provider/counter_provider.dart';
import 'package:provider/provider.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final counter = Provider.of<CounterProvider>(context);
    counter.count = 4;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          'press this many times',
          style: TextStyle(fontSize: 40),
        ),
        Text(
          counter.count.toString(),
          style: TextStyle(fontSize: 40),
        ),
      ],
    );
  }
}
