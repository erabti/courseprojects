import 'package:flutter/material.dart';
import 'package:future_app/provider/counter_provider.dart';
import 'package:future_app/ui/screen/main_screen.dart';
import 'package:future_app/ui/widgets/custom_floating_button.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ChangeNotifierProvider(
        builder: (_) => CounterProvider(),
        child: Scaffold(
          appBar: AppBar(),
          floatingActionButton: CustomFloatingButton(),
          body: Center(
            child: MainScreen(),
          ),
        ),
      ),
    );
  }
}


