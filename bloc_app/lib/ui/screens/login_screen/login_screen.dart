import 'package:bloc_app/bloc/auth/auth_bloc.dart';
import 'package:bloc_app/ui/screens/login_screen/login_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginScreen extends StatelessWidget {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: Text("Login")),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FlutterLogo(
                size: MediaQuery.of(context).size.width * 0.5,
              ),
              LogInText(
                  hintText: "username",
                  icon: Icons.account_circle,
                  controller: usernameController),
              LogInText(
                hintText: "password",
                icon: Icons.lock_outline,
                controller: passwordController,
              ),
              BlocBuilder<AuthBloc, AuthState>(
                builder: (context, state){
                  if(state is WrongPasswordError) return Text("Wrong Password");
                  if(state is NoUserError) return Text("No User");
                  return SizedBox();
                },
              ),
              RaisedButton(
                padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                child: Text(
                  "Login",
                  style: TextStyle(fontSize: 30, color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () {
                  print("Here");
                  BlocProvider.of<AuthBloc>(context).add(
                  SignInUser(usernameController.text, passwordController.text),
                );
                },
              )
            ],
          ),
        ),
      );
}
