import 'package:flutter/material.dart';

class LogInText extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final TextEditingController controller;

  const LogInText({Key key, this.hintText, this.icon, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 300,
        height: 50,
        child: TextField(
            decoration: InputDecoration(
          suffixIcon: Icon(icon),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(40),
          ),
          hintText: hintText,
        )),
      ),
    );
  }
}
