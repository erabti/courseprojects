import 'package:flutter/material.dart';

class MainScreen extends StatelessWidget {
  final String name;

  const MainScreen(this.name);
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text("Main Screen"),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.exit_to_app),
              onPressed: () {},
            )
          ],
        ),
        body: Center(
          child: Text("Welcome $name"),
        ),
      );
}
