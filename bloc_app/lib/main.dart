import 'package:bloc_app/bloc/auth/auth_bloc.dart';
import 'package:bloc_app/ui/screens/login_screen/login_screen.dart';
import 'package:bloc_app/ui/screens/main_screen.dart';
import 'package:bloc_app/ui/screens/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        home: BlocProvider(
          builder: (context) => AuthBloc(),
          child: BlocBuilder<AuthBloc, AuthState>(
            builder: (context, state) {
              if(state is Authenticated) return MainScreen(state.name);
              if(state is Unauthenticated || state is AuthError) return LoginScreen();
              return SplashScreen();
            },
          ),
        ),
      );
}
