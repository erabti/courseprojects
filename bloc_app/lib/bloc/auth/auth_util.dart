import 'package:shared_preferences/shared_preferences.dart';

const USERNAME_KEY = 'username';
const PASSWORD_KEY = 'password';
const NAME_KEY = 'name';
const LOGGED_IN_KEY = 'logged_in';

class AuthUtil {
  final instance = SharedPreferences.getInstance();
  Future<bool> signUp(String username, String password, String name) async {
    final instance = await this.instance;
    final _username = await instance.setString(USERNAME_KEY, username);
    final _password = await instance.setString(PASSWORD_KEY, password);
    final _name = await instance.setString(NAME_KEY, name);
    return _username && _password && _name;
  }

  Future<bool> isLoggedIn() async {
    final instance = await this.instance;
    final exist = instance.containsKey(LOGGED_IN_KEY);
    if (!exist) return false;
    return instance.getBool(LOGGED_IN_KEY);
  }


  Future<String> getName() async {
    final instance = await this.instance;
    return instance.getString(NAME_KEY);
  }

  Future<String> signIn(String username, String password) async {
    final instance = await this.instance;
    final usernameFieldExist = instance.containsKey(USERNAME_KEY);
    final _username = usernameFieldExist ? instance.getString(USERNAME_KEY) : null;
    if(!usernameFieldExist || _username != username) throw NoUserException();
    final _password = instance.getString(PASSWORD_KEY);
    if (password != _password) throw WrongPasswordException();
    instance.setBool(LOGGED_IN_KEY, true);
    return instance.get(NAME_KEY);
  }
//  Future<String> getName(String username, String password) async {
//
//    final pass = instance.getString(username);
//    if(pass != password) return null;
//    final name = instance.getString(username + NAME_KEY);
//    return name;
//  }
}


abstract class AuthException{}
class WrongPasswordException extends AuthException {}
class NoUserException extends AuthException{}