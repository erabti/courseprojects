abstract class AuthState {}

class Loading extends AuthState {}

class Authenticated extends AuthState {
  final String name;

  Authenticated(this.name);
}

class Unauthenticated extends AuthState {}

abstract class AuthError extends AuthState {}

class NoUserError extends AuthError {}

class WrongPasswordError extends AuthError {}
