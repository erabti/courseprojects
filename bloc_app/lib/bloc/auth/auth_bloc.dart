import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:bloc_app/bloc/auth/auth_event.dart';
import 'package:bloc_app/bloc/auth/auth_state.dart';
import 'package:bloc_app/bloc/auth/auth_util.dart';
export 'auth_event.dart';
export 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final util = AuthUtil();

  AuthBloc() {
    this.add(AppStarted());
  }
  @override
  AuthState get initialState => Loading();

  @override
  void onError(Object error, StackTrace stacktrace) {
    super.onError(error, stacktrace);
    print(error.toString() + ":" + stacktrace.toString());
  }

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    print(event);
    if (event is AppStarted) {
      yield Loading();
      final exist = await util.isLoggedIn();
      if (exist)
        yield Authenticated(await util.getName());
      else
        yield Unauthenticated();
    }
    if (event is SignInUser) {
      yield Loading();
      try {
        final name = await util.signIn(event.username, event.password);
        yield Authenticated(name);
      } catch(e){
        print(e);
        if(e is NoUserException) yield NoUserError();
        else yield WrongPasswordError();
      }
    }
  }
}
