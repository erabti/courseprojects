abstract class AuthEvent {}

class AppStarted extends AuthEvent {}

class SignInUser extends AuthEvent {
  final String username, password;
  SignInUser(this.username, this.password);
}

class SignUpUser extends AuthEvent {
  final String username, password, name;

  SignUpUser(this.username, this.password, this.name);
}

class SignOutUser extends AuthEvent {}

class ChangeName extends AuthEvent {
  final String newName, password;

  ChangeName(this.newName, this.password);
}
