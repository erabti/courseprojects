//المكتبة الرئيسية لFlutter تتبع تصميم ماتيريال من قوقل
import 'package:flutter/material.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  Widget build(BuildContext context) => MaterialApp( //ماتيريال اب يحتوي على الكثير من الويدجتس الجاهزة
      home: Scaffold( //تعمل كصفحة فارغة تتبعت تصميم ماتيريال
        backgroundColor: Colors.blue.shade300, //لون الخلفية
        body: Center( //ويدجت تقوم بوضع طفلها في وسط المساحة المسموح لها ان تتوسع فيها
          child: Text( //ويدجت تقوم بعرض نص معين
            "Hello World",
            style: TextStyle( //خاصية في ويدحت الText تتحكم بالستايل
              color: Colors.white, //لون النص
              fontSize: 30 //حجم النص
            ),
          ),
        ),
      ),
    );
}
