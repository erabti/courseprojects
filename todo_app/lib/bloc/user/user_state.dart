part of 'user_bloc.dart';

@immutable
abstract class UserState {}

class LoadingUser extends UserState {}

class UserFound extends UserState {
  final User user;
  UserFound(this.user);
}

class UserNotFound extends UserState {}
