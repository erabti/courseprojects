import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:todo_app/bloc/user/user_util.dart';
import 'package:todo_app/model/user.dart';

part 'user_event.dart';

part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final util = UserUtil();

  UserBloc() {
    add(CheckUser());
  }

  @override
  UserState get initialState => LoadingUser();

  @override
  void onError(Object error, StackTrace stacktrace) {
    super.onError(error, stacktrace);
    print(error.toString() + ": " + stacktrace.toString());
  }

  @override
  void onTransition(Transition<UserEvent, UserState> transition) {
    super.onTransition(transition);
    print(transition.toString());
  }

  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    if (event is CheckUser) {
      await Future.delayed(Duration(seconds: 2));
      final user = await util.getUser();
      if (user == null)
        yield UserNotFound();
      else
        yield UserFound(user);
    }
    if (event is ChooseUser) {
      if (event.user == null)
        yield UserNotFound();
      else
        yield UserFound(event.user);
      await util.setUser(event.user);
    }
  }
}
