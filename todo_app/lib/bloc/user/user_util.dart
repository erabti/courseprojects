import 'package:shared_preferences/shared_preferences.dart';
import 'package:todo_app/model/user.dart';

const TEACHER_VALUE = 'teacher';
const STUDENT_VALUE = 'student';
const USER_KEY = 'user';

class UserUtil {
  Future<User> getUser() async {
    final prefs = await SharedPreferences.getInstance();
    final result = prefs.getString(USER_KEY);
    return result == TEACHER_VALUE
        ? User.teacher
        : result == STUDENT_VALUE ? User.student : null;
  }

  Future<void> setUser(User user) async{
    final prefs = await SharedPreferences.getInstance();
    if(user == User.teacher) return prefs.setString(USER_KEY, TEACHER_VALUE);
    if(user == User.student) return prefs.setString(USER_KEY, STUDENT_VALUE);
    return prefs.remove(USER_KEY);
  }
}
