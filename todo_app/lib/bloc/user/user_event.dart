part of 'user_bloc.dart';

@immutable
abstract class UserEvent {}

class CheckUser extends UserEvent {}

class ChooseUser extends UserEvent{
  final User user;
  ChooseUser(this.user);
}