import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:todo_app/model/feedback.dart';

const FEEDBACK_COLLECTION = 'feedback';

class FeedbackUtil {
  Future<void> addFeedback(Feedback feedback) =>
      Firestore.instance.collection(FEEDBACK_COLLECTION).add(feedback.toMap());
}
