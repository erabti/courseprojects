import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:todo_app/bloc/feedback/feedback_util.dart';
import 'package:todo_app/model/feedback.dart';

part 'feedback_event.dart';

part 'feedback_state.dart';

class FeedbackBloc extends Bloc<FeedbackEvent, FeedbackState> {
  final util = FeedbackUtil();

  final BuildContext context;

  FeedbackBloc(this.context);

  @override
  void onError(Object error, StackTrace stacktrace) {
    super.onError(error, stacktrace);
    print(error.toString() + ": " + stacktrace.toString());
  }

  @override
  void onTransition(Transition<FeedbackEvent, FeedbackState> transition) {
    super.onTransition(transition);
    print(transition.toString());
  }

  @override
  FeedbackState get initialState => InitialFeedbackState();

  @override
  Stream<FeedbackState> mapEventToState(FeedbackEvent event) async* {
    if (event is AddFeedback) {
      yield AddingFeedback();
      try {
        await util.addFeedback(event.feedback);
        Flushbar(
          message: "Feedback Sent",
          duration: Duration(seconds: 2),
        ).show(context);
      } catch (e) {
        Flushbar(
          message: "Feedback Error",
          duration: Duration(seconds: 2),
        ).show(context);
      }
      event.controller.clear();
      yield InitialFeedbackState();
    }
  }
}
