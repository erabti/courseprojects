part of 'feedback_bloc.dart';

@immutable
abstract class FeedbackEvent {}

class AddFeedback extends FeedbackEvent {
  final Feedback feedback;
  final TextEditingController controller;

  AddFeedback(this.feedback, this.controller);
}

class FetchFeedback extends FeedbackEvent {}
