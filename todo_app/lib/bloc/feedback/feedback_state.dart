part of 'feedback_bloc.dart';

@immutable
abstract class FeedbackState {}

class InitialFeedbackState extends FeedbackState {}

class FetchingFeedback extends FeedbackState {}

class FetchedFeedback extends FeedbackState {
  final List<Feedback> feedback;

  FetchedFeedback(this.feedback);
}

class AddingFeedback extends FeedbackState {}
