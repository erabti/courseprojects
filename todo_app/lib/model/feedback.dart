import 'package:cloud_firestore/cloud_firestore.dart';

class Feedback {
  final String text;
  final Timestamp created;

  Feedback(this.text, this.created);

  Feedback.now(this.text) : this.created = Timestamp.now();

  Feedback.fromMap(Map<String, dynamic> map)
      : this.text = map['text'],
        this.created = map['created'];

  Map<String, dynamic> toMap() => {
        'text': text,
        'created': created,
      };
}
