import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/bloc/user/user_bloc.dart';
import 'package:todo_app/ui/screens/main_screen/main_screen.dart';
import 'package:todo_app/ui/screens/splash_screen.dart';
import 'package:todo_app/ui/screens/user_type_screen.dart';

class AppBase extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
        fontFamily: 'RobotoSlab'
    ),
    home: BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        if(state is UserNotFound) return UserTypeScreen();
        if(state is UserFound) return MainScreen();
        return SplashScreen();
      },
    ),
  );
}
