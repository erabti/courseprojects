import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  final double size;

  const Logo({Key key, this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) => Image.asset('assets/logo_transparent.png', width: size, height: size);
}
