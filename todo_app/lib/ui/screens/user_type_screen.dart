import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/bloc/user/user_bloc.dart';
import 'package:todo_app/model/user.dart';
import 'package:todo_app/ui/widgets/button.dart';
import 'package:todo_app/ui/widgets/logo.dart';

class UserTypeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<UserBloc>(context);
    return Scaffold(
        body: Column(
          children: <Widget>[
            Logo(),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    "What's your role at school?",
                    style: TextStyle(
                      fontSize: 25  ,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Button(
                    text: "Student",
                    onPressed: () => bloc.add(ChooseUser(User.student)),
                  ),
                  Button(
                    text: "Instructor",
                    onPressed: () => bloc.add(ChooseUser(User.teacher)),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
  }
}
