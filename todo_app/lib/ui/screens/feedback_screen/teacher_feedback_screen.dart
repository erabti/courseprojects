import 'package:flutter/material.dart';

class TeacherFeedbackScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Feedback"),
      ),
      body: Theme(
        data: ThemeData(
          textSelectionColor: Colors.white
        ),
        child: ListView(
          children: <Widget>[
            Card(
              color: Colors.blue,
              margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
              child: ListTile(
                title: Text("Hello there", style: TextStyle(fontSize: 17)),
                trailing: Text("18:12"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
