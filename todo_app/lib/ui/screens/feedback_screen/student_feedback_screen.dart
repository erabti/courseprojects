import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart' hide Feedback;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/bloc/feedback/feedback_bloc.dart';
import 'package:todo_app/model/feedback.dart';
import 'package:todo_app/ui/widgets/button.dart';
import 'package:todo_app/model/feedback.dart';

class StudentFeedbackScreen extends StatefulWidget {
  @override
  _StudentFeedbackScreenState createState() => _StudentFeedbackScreenState();
}

class _StudentFeedbackScreenState extends State<StudentFeedbackScreen> {
  final controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<FeedbackBloc>(context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "FeedBack",
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset("assets/complaintsAnimation.gif"),
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.height * 0.10,
                  horizontal: 10),
              child: TextField(
                controller: controller,
                decoration: InputDecoration(
                    hintText: "type your feedback here...",
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25))),
              ),
            ),
            BlocBuilder<FeedbackBloc, FeedbackState>(
              builder: (context, state) {
                return state is AddingFeedback
                    ? CircularProgressIndicator()
                    : Button(
                        onPressed: controller.text == ''
                            ? null
                            : () => bloc.add(AddFeedback(
                                Feedback.now(controller.text), controller)),
                        text: "Send",
                      );
              },
            )
          ],
        ),
      ),
    );
  }
}
