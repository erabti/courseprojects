import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/bloc/feedback/feedback_bloc.dart';
import 'package:todo_app/bloc/user/user_bloc.dart';
import 'package:todo_app/model/user.dart';
import 'package:todo_app/ui/screens/feedback_screen/student_feedback_screen.dart';
import 'package:todo_app/ui/screens/feedback_screen/teacher_feedback_screen.dart';

class FeedbackScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      builder: (context) => FeedbackBloc(context),
      child: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) {
          final userState = state as UserFound;
          if (userState.user == User.teacher)
            return TeacherFeedbackScreen();
          else
            return StudentFeedbackScreen();
        },
      ),
    );
  }
}
