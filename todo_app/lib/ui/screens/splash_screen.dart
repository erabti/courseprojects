import 'package:flutter/material.dart';
import 'package:todo_app/ui/widgets/logo.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Logo(),
            CircularProgressIndicator()
          ],
        ),
      );
}
