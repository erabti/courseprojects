import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/ui/screens/main_screen/main_screen.dart';

class Sheet extends StatefulWidget {
  @override
  _SheetState createState() => _SheetState();
}

class _SheetState extends State<Sheet> {
  TextEditingController controller;
  Priority priority;

  @override
  void initState() {
    priority = Priority.medium;
    controller = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) => SizedBox(
        height: MediaQuery.of(context).size.height * 0.6,
        child: Scaffold(
          body: SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.6,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    "Add new Task:",
                    style: Theme.of(context).textTheme.title,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    child: TextField(
                      controller: controller,
                      decoration: InputDecoration(
                          hintText: "Enter text...",
                          border: OutlineInputBorder()),
                    ),
                  ),
                  DropdownButton(
                    onChanged: (priority) =>
                        setState(() => this.priority = priority),
                    hint: Text("Select Priority..."),
                    value: priority,
                    items: [
                      _getDropDownItem("Urgent", Priority.urgent, Colors.red),
                      _getDropDownItem("Important", Priority.important,
                          Colors.blue.shade800),
                      _getDropDownItem(
                          "Medium", Priority.medium, Colors.greenAccent),
                      _getDropDownItem(
                          "Useless", Priority.useless, Colors.grey),
                    ],
                  ),
                  RaisedButton(
                    color: Colors.blue,
                    padding: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                    child: Text(
                      "Add",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 21,
                          color: Colors.white),
                    ),
                    onPressed: controller.text == '' ? null : (){},
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                  )
                ],
              ),
            ),
          ),
        ),
      );

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  DropdownMenuItem _getDropDownItem(
          String text, Priority priority, Color color) =>
      DropdownMenuItem(
        value: priority,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(text),
            Container(
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: color,
              ),
              height: 20,
              width: 20,
            )
          ],
        ),
      );
}
