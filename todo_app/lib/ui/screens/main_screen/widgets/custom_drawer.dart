import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/bloc/user/user_bloc.dart';
import 'package:todo_app/model/user.dart';
import 'package:todo_app/ui/screens/feedback_screen/feedback_screen.dart';
import 'package:todo_app/ui/screens/feedback_screen/student_feedback_screen.dart';
import 'package:todo_app/ui/widgets/logo.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<UserBloc>(context);
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.5,
      child: Drawer(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              DrawerHeader(
                padding: EdgeInsets.zero,
                child: Logo(),
              ),
              Container(
                child: BlocBuilder<UserBloc, UserState>(
                  builder: (context, state) {
                    final userState = state as UserFound;
                    return Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 25),
                          child: Image.asset(
                            "assets/${userState.user == User.teacher ? "teacher" : "student"}.png",
                            width: 45,
                          ),
                        ),
                        Text(
                          userState.user == User.teacher
                              ? "Teacher"
                              : "Student",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                      ],
                    );
                  },
                ),
                padding: EdgeInsets.all(10),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.3,
              ),
              DecoratedBox(
                decoration: BoxDecoration(color: Colors.blue.shade50),
                child: BlocBuilder<UserBloc, UserState>(
                  builder: (context, state) {
                    final userState = state as UserFound;
                    return ListTile(
                      onTap: () => Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => FeedbackScreen())),
                      title: Text(
                        "Feedback",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: 20,
              ),
              DecoratedBox(
                decoration: BoxDecoration(color: Colors.blue.shade50),
                child: ListTile(
                  title: Text(
                    "Contact Us",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: () => bloc.add(ChooseUser(null)),
                child: DecoratedBox(
                  decoration: BoxDecoration(color: Colors.blue.shade50),
                  child: ListTile(
                    title: Text(
                      "Log out",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
