import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/app_base.dart';
import 'package:todo_app/bloc/user/user_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) => BlocProvider<UserBloc>(
    builder: (context) => UserBloc(),
    child: AppBase(),
  );
}
