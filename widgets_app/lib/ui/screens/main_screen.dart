import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int count = 0;

  void setCount(int number) => setState(() => count = number);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Counter App"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.info),
            onPressed: () => Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => InfoScreen())),
          )
        ],
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          FloatingActionButton(
            heroTag: 'add',
            child: Icon(Icons.add),
            backgroundColor: Colors.green,
            onPressed: () => setCount(count + 1),
          ),
          FloatingActionButton(
            heroTag: 'reset',
            backgroundColor: Colors.yellow.shade800,
            child: Icon(Icons.refresh),
            onPressed: () => setCount(0),
          ),
          FloatingActionButton(
            heroTag: 'minus',
            child: Icon(Icons.remove),
            onPressed: () => setCount(count - 1),
            backgroundColor: Colors.red,
          )
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('You pressed the button this times:'),
            Text(count.toString(), style: TextStyle(fontSize: 34))
          ],
        ),
      ),
    );
  }
}

class InfoScreen extends StatelessWidget {
  InfoScreen({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Info Page"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Bla Bla"),
            RaisedButton(
              onPressed: () {
                Navigator.of(context).maybePop();              },
              child: Text("Go Back!"),
            )
          ],
        ),
      ),
    );
  }
}
