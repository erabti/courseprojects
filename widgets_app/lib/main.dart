import 'package:flutter/material.dart';
import 'package:widgets_app/ui/screens/main_screen.dart';

void main() => runApp(AppBase());

class AppBase extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          textTheme: TextTheme(
            body1: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          ),
        ),
        home: MainScreen(),
      );
}
